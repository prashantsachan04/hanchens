// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:hanchens/screen/beta_screens/beta_home_screen.dart';
import 'package:hanchens/screen/camera/front_image.dart';
import 'package:hanchens/screen/login_screen/login_check.dart';
import 'package:hanchens/screen/about_us/about_us.dart';
import 'package:hanchens/screen/current_location/current_location.dart';
import 'package:hanchens/screen/live_location/live_location.dart';
import 'package:hanchens/screen/login_screen/login_screen.dart';
import 'package:hanchens/screen/onboarding/onboarding_screen.dart';
import 'package:hanchens/screen/otp_screen/otp_screen.dart';
import 'package:hanchens/screen/vendor_page/vender_page.dart';
import './screen/live_location/live_location.dart';

final Map<String, WidgetBuilder> routes = {
  VendorPage.routeName: (context) => VendorPage(),
  // Homescreen.routeName: (context) => Homescreen(),
  // LoginScreen.routeName: (context) => LoginScreen(),
  LiveLocation.routeName: (context) => LiveLocation(),
  CurrentLocation.routeName: (context) => CurrentLocation(),
  AboutUs.routeName: (context) => AboutUs(),
  InitializerWidget.routeName: (context) => InitializerWidget(),
  OnboardingScreen.routeName: (context) => OnboardingScreen(),
  LoginScreenGetX.routeName: (context) => LoginScreenGetX(),
  OtpScreen.routeName: (context) => OtpScreen(),
  ImageWidget.routeName: (context) => ImageWidget(),
  BetaHomescreen.routeName: (context) => BetaHomescreen(),
};
