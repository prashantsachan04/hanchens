import 'package:geolocator/geolocator.dart';
import 'package:hanchens/Assitants/request_assitant.dart';

class AssitantMethod {
  Future<List<String>> searchCoordinateAddress(Position position) async {
    
    List<String> data = [];
    String placeAddress = "";
    String completeAddress = "";

    var url = Uri.parse(
        "https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.latitude},${position.longitude}&key=AIzaSyDoEhP8BnHQ2JxNleGih8FDINqwO-A4Q3U");

    var response = await ResquestAssistant.getRequest(url);

    if (response != "failed") {
      placeAddress =
          response["results"][0]["address_components"][4]["long_name"];

      completeAddress = response["results"][0]["formatted_address"];
      data.add(placeAddress);
      data.add(completeAddress);
    }
    return data;
  }
}
