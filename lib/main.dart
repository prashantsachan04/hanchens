import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:hanchens/screen/OCR/ocr.dart';
import 'package:hanchens/screen/beta_screens/beta_home_screen.dart';
import 'package:hanchens/screen/create_account/create_account_screen1.dart';
import 'package:hanchens/screen/create_account/create_account_screen2.dart';
import 'package:hanchens/screen/login_screen/login_check.dart';
import 'package:hanchens/screen/login_screen/login_screen.dart';
import 'package:hanchens/screen/onboarding/onboarding_screen.dart';
import 'package:hanchens/screen/profile_screen/profile.dart';

import 'package:hanchens/theme.dart';
import './routes.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'screen/create_account/create_account_screen3.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
}

class MyApp extends StatelessWidget {
  static String routeName = "/help";

  const MyApp({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      builder: () {
        return GetMaterialApp(
            theme: theme(),
            debugShowCheckedModeBanner: false,
            title: 'Hanchens',
            home: InitializerWidget(),
            // initialRoute: OnboardingScreen.routeName,
            routes: routes);
      },
    );
  }
}
