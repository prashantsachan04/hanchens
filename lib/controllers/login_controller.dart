import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController{
  String mobile;
  final phoneController = TextEditingController();
  final otpController = TextEditingController();
  String verificationId;

  String validateMobile(String value) {
    String patttern = r'(^[0-9]*$)';
    RegExp regExp = RegExp(patttern);
    if (value.isEmpty) {
      return "Mobile is Required";
    } else if (value.length != 10) {
      return "Mobile number must 10 digits";
    } else if (!regExp.hasMatch(value)) {
      return "Mobile Number must be digits";
    }
    return null;
  }

  
}