import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class CreateAccountController extends GetxController {
  RxString verificationDoc = 'Pan card'.obs;

  final String personalDetails = '01/04: Personal details';

  final String name = 'NAME';
  final String email = 'EMAIL ID';
  final String mobile = 'MOBILE NUMBER';
  final String permanentAddress = 'PERMANENT ADDRESS';
  final String currentAddress = 'CURRENT ADDRESS';
  final String aadhaar = 'AADHAAR NUMBER';
  final String pan = 'PAN NUMBER';
  final String policVerification = 'POLICE VERIFICATION';
  var list = <String>[
    'Pan card',
    'Aadhaar',
  ].map<DropdownMenuItem<String>>((String value) {
    return DropdownMenuItem<String>(
      value: value,
      child: Text(value),
    );
  });

  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final mobileController = TextEditingController();
  final permanentAddressController = TextEditingController();
  final currentAddressController = TextEditingController();
  final aadhaarController = TextEditingController();
  final panController = TextEditingController();

// date picker function and variables
  DateTime selectedInsuranceExpiryDate = DateTime.now();
  DateTime selectedDlExpiryDate = DateTime.now();
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final RxString formattedInsuranceExpiryDate = 'Select date'.obs;
  final RxString formattedDlExpiryDate = 'Select date'.obs;

  Future<void> selectDate(BuildContext context, String doctype) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedInsuranceExpiryDate,
        firstDate: DateTime.now(),
        lastDate: DateTime(2101));
    // ignore: unrelated_type_equality_checks
    if (picked != null && picked != selectedInsuranceExpiryDate) {
      doctype == 'insurance'
          ? selectedInsuranceExpiryDate = picked
          : selectedDlExpiryDate = picked;
      doctype == 'insurance'
          ? formattedInsuranceExpiryDate.value =
              formatter.format(selectedInsuranceExpiryDate)
          : formattedDlExpiryDate.value =
              formatter.format(selectedDlExpiryDate);
    }
  }

  // create account 2nd screen
  final String vehiclesDetails = '02/04: Vehicle details';
  final String vehicleNumber = 'VEHICLE NUMBER';
  final String rcNumber = 'RC NUMBER';
  final String insuraceExpiryDate = 'INSURANCE EXPIRY DATE';
  final String dlExpiryDate = 'DL EXPIRY DATE';
  final String vehicleType = "VEHICLE TYPE";
  final String deliveryIntensity = "DELIVERY INTENSITY";

  final vehicleNumberController = TextEditingController();
  final rcNumberController = TextEditingController();
  final vehicleTypeController = TextEditingController();

  // create account 3rd screen
  final String workDetails = '03/04: Work details';

  final companyName = 'COMPANY NAME';
  final experienceYears = 'EXPERIENCE';

  final companyNameController = TextEditingController();
  final experienceYearsController = TextEditingController();
}
