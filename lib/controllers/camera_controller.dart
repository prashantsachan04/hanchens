import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class CameraController extends GetxController {
  var selectedImagePath = ''.obs;
  var selectedBackImagepath = ''.obs;

  void getImage(ImageSource imageSource, String image) async {
    final pickedFile = await ImagePicker().pickImage(source: imageSource);
    if (pickedFile != null) {
      image == 'frontImage'
          ? selectedImagePath.value = pickedFile.path
          : selectedBackImagepath.value = pickedFile.path;
    }
  }
}
