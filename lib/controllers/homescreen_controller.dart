import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hanchens/Assitants/assistant_method.dart';
import 'package:hanchens/screen/beta_screens/beta_home_screen.dart';
import 'package:hanchens/screen/login_screen/login_screen.dart';
import 'package:http/http.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:intl/intl.dart';

class HomeScreenController extends GetxController {
  RxBool status = false.obs;
  RxString driverStatus = 'Offline'.obs;
  RxString todaysDate = ''.obs;
  DateTime date = DateTime.now();
  final DateFormat formatter = DateFormat('dd');
  var permission = Geolocator.checkPermission();

  // CameraPosition kGooglePlex = const CameraPosition(
  //   target: LatLng(37.4279879798, -122.085797340), zoom: 14);

  RxString titleData = 'Location'.obs;
  RxString completeData = 'Current location will be seen here'.obs;

  /// final Completer<GoogleMapController> controllerGoogleMap = Completer();
  // GoogleMapController newGoogleMapController;
  //Position currentPosition;
  Geolocator geolocator = Geolocator();
  //Set<Marker> markers = {};

  void locatePosition() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    //LatLng latLngPosition = LatLng(position.latitude, position.longitude);

    //  CameraPosition cameraPosition =
    //    CameraPosition(target: latLngPosition, zoom: 14);
    List<String> address =
        await AssitantMethod().searchCoordinateAddress(position);
    [];
    titleData.value = address[0];
    completeData.value = address[1];

    //  newGoogleMapController
    //    .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));

    //  markers.add(Marker(
    //    markerId: const MarkerId('id-1'),
    //  position: latLngPosition,
    //infoWindow: const InfoWindow(
    //  title: 'Data from api', snippet: 'Data from api')));
  }

  permisionCheck() async {
    if (await Permission.location.isDenied) {
      // locatePosition();
      alertBox();
      // We didn't ask for permission yet or the permission has been denied before but not permanently.
    }

// You can can also directly ask the permission about its status.
    if (await Permission.location.status.isGranted) {
      locatePosition();
      // The OS restricts access, for example because of parental controls.
    }
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    todaysDate.value = formatter.format(date);

    Permission.location.request();
    permisionCheck();
  }
}
