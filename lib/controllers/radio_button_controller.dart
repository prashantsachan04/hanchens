import 'package:get/get.dart';

class RadioButtonController extends GetxController {
  RxInt button1 = 1.obs;
  RxInt button2 = 1.obs;

  pressButton1() {
    button1.value = 1;
    button2.value = 0;
  }

  pressButton2() {
    button1.value = 0;
    button2.value = 1;
  }
}
