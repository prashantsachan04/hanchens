import 'dart:async';

import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:get/get.dart';
import 'package:hanchens/Assitants/assistant_method.dart';

class MapController extends GetxController {
  CameraPosition kGooglePlex = const CameraPosition(
      target: LatLng(37.4279879798, -122.085797340), zoom: 14);

  String titleData = 'Location';
  String completeData = '';
  final Completer<GoogleMapController> controllerGoogleMap = Completer();
  GoogleMapController newGoogleMapController;
  Position currentPosition;
  Geolocator geolocator = Geolocator();
  Set<Marker> markers = {};

  void locatePosition() async {
    refresh();
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    LatLng latLngPosition = LatLng(position.latitude, position.longitude);

    CameraPosition cameraPosition =
        CameraPosition(target: latLngPosition, zoom: 14);
        List<String> address =
        await AssitantMethod().searchCoordinateAddress(position);
    [];
    titleData = address[0];
    completeData = address[1];

    newGoogleMapController
        .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));

    markers.add(Marker(
        markerId: const MarkerId('id-1'),
        position: latLngPosition,
        infoWindow: const InfoWindow(
            title: 'Data from api', snippet: 'Data from api')));

 
  }
}

/*Marker(
          markerId: MarkerId('id-1'),
          position: latLngPosition,
          infoWindow:
              InfoWindow(title: 'Data from api', snippet: 'Data from api'))*/