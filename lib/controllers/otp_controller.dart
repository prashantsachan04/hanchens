import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:hanchens/controllers/homescreen_controller.dart';
import 'package:hanchens/screen/beta_screens/beta_home_screen.dart';
import 'package:hanchens/screen/otp_screen/otp_screen.dart';
import 'package:hanchens/screen/current_location/current_location.dart';

class OtpController extends GetxController {
  String code = '';
  final FirebaseAuth auth = FirebaseAuth.instance;
  void signInWithPhoneAuthCredential(
      PhoneAuthCredential phoneAuthCredential) async {
    try {
      final authCredential =
          await auth.signInWithCredential(phoneAuthCredential);
      Get.to(OtpScreen());

      if (authCredential?.user != null) {
        Get.to(()=> BetaHomescreen());
      }
    } on FirebaseAuthException catch (e) {
      Get.snackbar('error', e.message);
    }
  }
}
