import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hanchens/constants.dart';

class Profile extends StatelessWidget {
  const Profile({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[50],
      body: Column(
        children: [
          Header(),
          ProfileOption(
            title: 'Personal Information',
            data: 'Manage Your name, Mobile number, Email address',
            icon: Icons.account_circle,
          ),
          ProfileOption(
            title: 'Vehicle Details',
            data: 'Manage Your name, Mobile number, Email address',
            icon: Icons.directions_bike_outlined,
          ),
          ProfileOption(
              title: 'Work Information',
              data: 'Manage Your name, Mobile number, Email address',
              icon: Icons.alarm_sharp),
          ProfileOption(
            title: 'Personal Information',
            data: 'Manage Your name, Mobile number, Email address',
            icon: Icons.calendar_today_sharp,
          ),
          SizedBox(
            height: 10.h,
          )
        ],
      ),
    );
  }
}

class ProfileOption extends StatelessWidget {
  ProfileOption({this.title, this.data, this.icon});

  String title;
  String data;
  IconData icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.w),
      height: 90.h,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Icon(
            icon,
            size: 50,
          ),
          SizedBox(
            width: 250.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Text(data, style: TextStyle(color: Colors.grey[400])),
              ],
            ),
          ),
          Icon(Icons.navigate_next)
        ],
      ),
    );
  }
}

class Header extends StatelessWidget {
  const Header({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150.h,
      color: Colors.green[300],
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          CircleAvatar(
            child: Icon(
              Icons.account_circle_outlined,
              size: 50,
            ),
            radius: 30,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Captains name',
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.white60,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 5.h,
              ),
              Text(
                'Roshan Singh',
                style: TextStyle(
                    fontSize: 25,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 5.h,
              ),
              Text(
                'ID 11233425243',
                style: TextStyle(
                    fontSize: 17,
                    color: Colors.white60,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Icon(
                  Icons.star,
                  color: Colors.yellow[800],
                ),
                Text(
                  '4.5',
                  style: TextStyle(fontWeight: FontWeight.w900),
                )
              ],
            ),
            width: 70,
            height: 40,
            color: Colors.white,
          )
        ],
      ),
    );
  }
}
