import 'package:flutter/material.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

class Body extends StatelessWidget {
  const Body({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const FloatingSearchAppBar(
            body: null,
            elevation: 6,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                const TextButton(
                  onPressed: null,
                  child: Text('vendors'),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.05,
                ),
                const TextButton(
                  onPressed: null,
                  child: Text('Products'),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
