import 'package:flutter/material.dart';
import 'package:hanchens/screen/vendor_page/component/body.dart';

class VendorPage extends StatelessWidget {
  static String routeName = "/vendor";
  const VendorPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Body(),
    );
  }
}
