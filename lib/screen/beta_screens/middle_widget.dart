
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../constants.dart';

class Middle extends StatelessWidget {
  const Middle({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.w),
      margin: EdgeInsets.symmetric(horizontal: 12.w),
      height: 260.h,
      decoration: BoxDecoration(
        color: Colors.white38,
        borderRadius: BorderRadius.all(
          Radius.circular(20.r),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text('Past Orders',
                  style: TextStyle(
                      fontSize: 18.sp,
                      color: Colors.black54,
                      fontWeight: FontWeight.bold)),
              SizedBox(
                height: 5.h,
              ),
              Text(
                'Check your completed orders',
                style: textHeadingStyle,
              )
            ],
          ),
          Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10.r))),
              child: IconButton(
                  onPressed: () {}, icon: Icon(Icons.navigate_next_outlined)))
        ],
      ),
    );
  }
}