import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hanchens/controllers/homescreen_controller.dart';
import 'package:hanchens/controllers/login_controller.dart';
import 'package:hanchens/screen/beta_screens/header.dart';
import 'package:hanchens/screen/beta_screens/top_widget.dart';
import 'package:permission_handler/permission_handler.dart';

import 'bottom_widget.dart';
import 'middle_widget.dart';

// ignore: must_be_immutable
class BetaHomescreen extends StatelessWidget {
  static String routeName = "/beta_home_screen";
  final LoginController loginController = Get.put(LoginController());
  BetaHomescreen({Key key}) : super(key: key);

  HomeScreenController homeScreenController = Get.put(HomeScreenController());
  // CameraController cameraController = Get.put(CameraController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[50],
      body: Padding(
        padding: EdgeInsets.zero,
        child: Column(
          children: [
            Header(),
            SizedBox(
              height: 20,
            ),
            Stack(
              alignment: Alignment.topCenter,
              children: [
                Bottom(),
                Middle(),
                Top(
                    homeScreenController: homeScreenController,
                    loginController: loginController),
              ],
            ),
            SizedBox(
              height: 130.h,
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40.w),
              color: Colors.black,
              height: 40.h,
              width: double.infinity,
              child: Text(
                'COMPLETE DUTY',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
            )
          ],
        ),
      ),
    );
  }
}

Future<dynamic> alertBox() {
  HomeScreenController homeScreenController = Get.put(HomeScreenController());
  return Get.dialog(
    Center(
      child: Material(
        child: Container(
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(20))),
          margin: EdgeInsets.symmetric(horizontal: 20),
          padding: EdgeInsets.symmetric(horizontal: 30),
          height: 200,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Location permission is required to access this app',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 20.h,
              ),
              TextButton(
                  onPressed: () {
                    Permission.location.request();

                    Future.delayed(Duration(seconds: 2), () async {
                      if (await Permission.location.status.isGranted) {
                        Get.back();
                        homeScreenController.locatePosition();
                      }
                    });
                  },
                  child: Container(
                      padding: EdgeInsets.all(10),
                      alignment: Alignment.center,
                      // height: 30.h,
                      //  width: 100.w,
                      color: Colors.green[300],
                      child: Text(
                        'Enable location',
                        style: TextStyle(fontSize: 16, color: Colors.white),
                      )))
            ],
          ),
        ),
      ),
    ),
    barrierDismissible: false,
  );
}
