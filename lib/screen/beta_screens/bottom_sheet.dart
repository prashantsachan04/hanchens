import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hanchens/controllers/login_controller.dart';
import 'package:hanchens/controllers/otp_controller.dart';
import 'package:hanchens/screen/create_account/components/create_account_button.dart';
import 'package:sms_autofill/sms_autofill.dart';

import '../../constants.dart';

class BottomScreen extends StatelessWidget {
  const BottomScreen({
    Key key,
    @required this.loginController,
  }) : super(key: key);

  final LoginController loginController;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: 300,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text('Enter OTP', style: headingStyle),
            Text(
              'If you want to be Online enter your OTP',
              style: textHeadingStyle,
            ),
            PinFieldAutoFill(
              codeLength: 6,
              controller: loginController.otpController,
              decoration: UnderlineDecoration(
                textStyle: TextStyle(fontSize: 20, color: Colors.black),
                colorBuilder: FixedColorBuilder(Colors.black.withOpacity(0.3)),
              ),
              onCodeSubmitted: (_) {
                PhoneAuthCredential phoneAuthCredential =
                    PhoneAuthProvider.credential(
                        verificationId: loginController.verificationId,
                        smsCode: loginController.otpController.text);

                OtpController()
                    .signInWithPhoneAuthCredential(phoneAuthCredential);
              },
            ),
            CreateAccountButton(
              onPressed: () {},
              text: 'Online',
            ),
            Text(
              'If you enter your otp wrong for 5 times your account  \n will be blocked',
              style: TextStyle(
                color: Colors.red,
              ),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}
