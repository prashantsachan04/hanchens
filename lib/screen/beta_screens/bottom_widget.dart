

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hanchens/controllers/homescreen_controller.dart';

class Bottom extends StatelessWidget {
  HomeScreenController homeScreenController = Get.find<HomeScreenController>();
  Bottom({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Obx(
            () => Text(
              homeScreenController.titleData.value,
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            ),
          ),
          Obx(
            () => Text(homeScreenController.completeData.value,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 14.sp, color: Colors.blue)),
          ),
          SizedBox(
            height: 10.h,
          ),
          Text('Note - To get the new orders stay inside the zone always')
        ],
      ),
      margin: EdgeInsets.symmetric(horizontal: 10.w),
      height: 350.h,
      decoration: BoxDecoration(
          color: Colors.white38,
          borderRadius: BorderRadius.all(Radius.circular(20.r))),
    );
  }
}
