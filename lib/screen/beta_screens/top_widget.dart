import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hanchens/controllers/homescreen_controller.dart';
import 'package:hanchens/controllers/login_controller.dart';
import 'package:hanchens/screen/beta_screens/beta_home_screen.dart';
import 'package:hanchens/screen/beta_screens/bottom_sheet.dart';

class Top extends StatelessWidget {
  const Top({
    Key key,
    @required this.homeScreenController,
    @required this.loginController,
  }) : super(key: key);

  final HomeScreenController homeScreenController;
  final LoginController loginController;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Obx(
                () => Text(
                  'You are ${homeScreenController.driverStatus.value}',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                ),
              ),
              Obx(
                () => Switch(
                  activeColor: Colors.green[300],
                  value: homeScreenController.status.value,
                  onChanged: (value) {
                    homeScreenController.status.value = value;

                    if (value == true) {
                      homeScreenController.driverStatus.value = 'Online';
                      Get.bottomSheet(
                          BottomScreen(loginController: loginController));
                    } else {
                      homeScreenController.driverStatus.value = 'Offline';
                    }
                  },
                ),
              ),
            ],
          ),
          Obx(
            () => Text(
              homeScreenController.todaysDate.value,
              style: TextStyle(fontSize: 60.sp, fontWeight: FontWeight.w900),
            ),
          ),
          Text(
            "Today,s completed Orders",
            style: TextStyle(
                color: Colors.grey, fontWeight: FontWeight.w500, fontSize: 20),
          )
        ],
      ),
      padding: EdgeInsets.all(10.w),
      margin: EdgeInsets.symmetric(horizontal: 12.w),
      height: 200.h,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(20.r),
        ),
      ),
    );
  }
}
