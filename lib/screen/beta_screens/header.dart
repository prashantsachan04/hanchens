import 'package:flutter/material.dart';
import 'package:flutter_mobile_vision/flutter_mobile_vision.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hanchens/controllers/camera_controller.dart';
import 'package:hanchens/screen/OCR/ocr.dart';
import 'package:hanchens/screen/login_screen/login_screen.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../../constants.dart';

class Header extends StatelessWidget {
  final _auth = FirebaseAuth.instance;
  Header({
    Key key,
  }) : super(key: key);

  int _ocrCamera = FlutterMobileVision.CAMERA_BACK;
  Future<Null> _read() async {
    List<OcrText> texts = [];
    try {
      texts = await FlutterMobileVision.read(
          camera: _ocrCamera, waitTap: false, multiple: true, autoFocus: true);
    } on Exception {
      texts.add(OcrText('Failed to recognize text'));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Hello,',
                style: textHeadingStyle,
              ),
              Text(
                'Roshan Singh',
                style: headingStyle,
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              IconButton(
                  onPressed: () {
                    Get.to(MainPage());
                  },
                  icon: Icon(Icons.qr_code_scanner)),
              Stack(
                alignment: Alignment.topRight,
                children: [
                  Icon(
                    Icons.notifications,
                    size: 30,
                  ),
                  Container(
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(1000))),
                    // color: Colors.white,
                    padding: const EdgeInsets.all(2.0),
                    child: CircleAvatar(
                      backgroundColor: Colors.green[300],
                      radius: 07.r,
                      child: Text(
                        '05',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 9,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              ),
              IconButton(
                onPressed: () async {
                  await _auth.signOut();
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LoginScreenGetX()));
                },
                icon: Icon(
                  Icons.logout_rounded,
                  size: 25,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
