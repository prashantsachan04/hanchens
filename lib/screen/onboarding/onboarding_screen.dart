// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hanchens/screen/login_screen/login_check.dart';
import 'package:introduction_screen/introduction_screen.dart';

// ignore: must_be_immutable
class OnboardingScreen extends StatelessWidget {
  static String routeName = "/onboard";

  List<PageViewModel> listPagesViewModel = [
    PageViewModel(
      title: "Lorem ipsum",
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      image: Image.asset(
        'assets/images/splash_1.png',
      ),
      decoration: PageDecoration(),
      footer: ElevatedButton(
        onPressed: () {
          // On button presed
        },
        child: const Text("Let's Go !"),
      ),
    ),
    PageViewModel(
      title: "Lorem ipsum",
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      image: Image.asset(
        'assets/images/splash_3.png',
        height: 350.h,
      ),
    ),
    PageViewModel(
      title: "Lorem ipsum",
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      image: Image.asset(
        'assets/images/splash_2.png',
        height: 350.h,
      ),
    ),
  ];

  OnboardingScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IntroductionScreen(
        pages: listPagesViewModel,
        onDone: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => InitializerWidget()));
        },
        showNextButton: true,
        showDoneButton: true,
        showSkipButton: true,
        skip: const Text("Skip"),
        onSkip: () {
          Get.to(InitializerWidget());
        },
        done: const Text(
          "Done",
          style: TextStyle(fontWeight: FontWeight.w600),
        ),
        next: Text("next"),
      ),
    );
  }
}
