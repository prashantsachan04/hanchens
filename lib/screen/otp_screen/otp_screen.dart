// ignore_for_file: prefer_const_constructors
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hanchens/controllers/login_controller.dart';
import 'package:hanchens/controllers/otp_controller.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class OtpScreen extends StatelessWidget {
  static String routeName = "/otp";
  //final LoginController loginController = Get.put(LoginController());
  //final OtpController otpController = Get.put(OtpController());

  final LoginController loginController = Get.find<LoginController>();
  final OtpController otpController = Get.find<OtpController>();

  OtpScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.w),
          child: Column(
            children: [
              Container(
                child: BackButton(),
                width: double.infinity,
                alignment: Alignment.centerLeft,
              ),
              SizedBox(
                height: 50.h,
              ),
              Text(
                'OTP Verification',
                style: GoogleFonts.roboto(
                    fontSize: 40, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20.h,
              ),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                    text: 'Check your message box we just sent you an\n',
                    style:
                        TextStyle(color: Colors.grey, fontSize: 18, height: 2),
                    children: <TextSpan>[
                      TextSpan(
                        text: '4 - digit code at ',
                        style: TextStyle(color: Colors.grey, fontSize: 18),
                      ),
                      TextSpan(
                        text: (' +91 ' + loginController.phoneController.text),
                        style: TextStyle(color: Colors.black, fontSize: 18),
                      )
                    ]),
              ),
              SizedBox(
                height: 70.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: PinFieldAutoFill(
                  codeLength: 6,
                  controller: loginController.otpController,
                  decoration: UnderlineDecoration(
                    textStyle: TextStyle(fontSize: 20, color: Colors.black),
                    colorBuilder:
                        FixedColorBuilder(Colors.black.withOpacity(0.3)),
                  ),
                  onCodeSubmitted: (_) {
                    PhoneAuthCredential phoneAuthCredential =
                        PhoneAuthProvider.credential(
                            verificationId: loginController.verificationId,
                            smsCode: loginController.otpController.text);

                    OtpController()
                        .signInWithPhoneAuthCredential(phoneAuthCredential);
                  },
                ),
              ),
              SizedBox(
                height: 20.h,
              ),
              TextButton(
                onPressed: () async {
                  PhoneAuthCredential phoneAuthCredential =
                      PhoneAuthProvider.credential(
                          verificationId: loginController.verificationId,
                          smsCode: loginController.otpController.text);

                  otpController
                      .signInWithPhoneAuthCredential(phoneAuthCredential);
                },
                child: Container(
                  height: 50.h,
                  width: double.infinity,
                  alignment: Alignment.center,
                  color: Colors.green[300],
                  child: Text(
                    'Verify now',
                    style: TextStyle(color: Colors.white, fontSize: 20.sp),
                  ),
                ),
              ),
              Text(
                "Didn't you receive any code ?",
                style: TextStyle(color: Colors.grey, fontSize: 18, height: 2),
              ),
              TextButton(
                onPressed: () {},
                child: Text(
                  "Re-send code",
                  style: TextStyle(color: Colors.black, fontSize: 18),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
