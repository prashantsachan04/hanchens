import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hanchens/controllers/map_controller.dart';

class Mapscreen extends StatefulWidget {
  const Mapscreen({Key key}) : super(key: key);

  @override
  State<Mapscreen> createState() => _MapscreenState();
}

class _MapscreenState extends State<Mapscreen> {
  MapController mapController = Get.put(MapController());
  

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: TextButton(
          onPressed: () {
            mapController.locatePosition();
            
          },
          child: const Text('data'),
        ),
      ),
      body: GoogleMap(
        markers: mapController.markers,
        initialCameraPosition: mapController.kGooglePlex,
        myLocationButtonEnabled: true,
        onMapCreated: (GoogleMapController controller) {
          mapController.controllerGoogleMap.complete(controller);
          mapController.newGoogleMapController = controller;
        },
      ),
    );
  }
}
