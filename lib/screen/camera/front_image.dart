import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hanchens/controllers/camera_controller.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dotted_border/dotted_border.dart';

class ImageWidget extends StatelessWidget {
  final CameraController cameraController = Get.find<CameraController>();

  static String routeName = '/camera';

  ImageWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Obx(
          () => cameraController.selectedImagePath.value == ''
              ? DottedBorder(
                  borderType: BorderType.RRect,
                  radius: Radius.circular(20.r),
                  child: Container(
                      alignment: Alignment.center,
                      height: 150.h,
                      width: 150.w,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IconButton(
                              onPressed: () {
                                print(cameraController.selectedImagePath.value);
                                cameraController.getImage(
                                    ImageSource.gallery, 'frontImage');
                              },
                              icon: const Icon(Icons.image)),
                          const Text('Vehicle image Front')
                        ],
                      )))
              : SizedBox(
                  height: 150.h,
                  width: 150.w,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Stack(
                      alignment: Alignment.topRight,
                      children: [
                        Image.file(
                          File(cameraController.selectedImagePath.value),
                          width: double.infinity,
                          height: 300,
                          fit: BoxFit.cover,
                        ),
                        IconButton(
                            onPressed: () {
                              cameraController.selectedImagePath.value = '';
                            },
                            icon: const Icon(
                              Icons.cancel,
                              color: Colors.white,
                            ))
                      ],
                    ),
                  ),
                ),
        ),
      ],
    );
  }
}
