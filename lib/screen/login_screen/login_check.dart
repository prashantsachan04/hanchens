// ignore_for_file: prefer_const_constructors
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hanchens/screen/beta_screens/beta_home_screen.dart';
import 'package:hanchens/screen/current_location/current_location.dart';
import 'package:hanchens/screen/login_screen/login_screen.dart';

class InitializerWidget extends StatefulWidget {
  static String routeName = "/MockOTP";

  const InitializerWidget({Key key}) : super(key: key);
  @override
  _InitializerWidgetState createState() => _InitializerWidgetState();
}

class _InitializerWidgetState extends State<InitializerWidget> {
  FirebaseAuth _auth;

  User _user;

  bool isLoading = true;

  @override
  void initState() {
    
    super.initState();
    _auth = FirebaseAuth.instance;
    _user = _auth.currentUser;
    isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          )
        : _user == null
            ? LoginScreenGetX()
            : BetaHomescreen();
  }
}
