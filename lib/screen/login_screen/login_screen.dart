// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hanchens/controllers/login_controller.dart';
import 'package:hanchens/controllers/otp_controller.dart';
import 'package:hanchens/screen/otp_screen/otp_screen.dart';
import 'package:sms_autofill/sms_autofill.dart';

class LoginScreenGetX extends StatelessWidget {
  static String routeName = '/login_getx';
  final LoginController loginController = Get.put(LoginController());
  final OtpController otpController = Get.put(OtpController());

  LoginScreenGetX({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.w),
          child: Column(
            children: [
              SizedBox(
                height: 10.h,
              ),
              Padding(
                padding: EdgeInsets.only(right: 20.w),
                child: Image.asset(
                  'assets/images/hanchens.png',
                  height: 350.h,
                ),
              ),
              SizedBox(
                height: 20.h,
              ),
              Text(
                "What's your Mobile \nnumber ?",
                textAlign: TextAlign.center,
                style: GoogleFonts.roboto(
                  textStyle: TextStyle(
                      color: Colors.black,
                      letterSpacing: 1,
                      fontSize: 35,
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 20.h,
              ),
              Text(
                'Please enter your mobile number to vereify your \naccount',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.blueGrey, fontSize: 15.sp),
              ),
              SizedBox(
                height: 20.h,
              ),
              TextFormField(
                  controller: loginController.phoneController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: TextStyle(color: Colors.grey),

                    hintText: '    mobile number', prefixText: '+91-',

                    prefixStyle: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w900),
                    filled: true,
                    //fillColor: Colors.grey,
                    contentPadding:
                        EdgeInsets.only(left: 14.w, bottom: 6.0.w, top: 8.0.w),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(),
                      borderRadius: BorderRadius.circular(10.0.r),
                    ),
                  ),
                  keyboardType: TextInputType.phone,

                  // initialValue: '+91',
                  style: TextStyle(fontWeight: FontWeight.bold),
                  maxLength: 10,
                  validator: loginController.validateMobile,
                  onSaved: (String val) {
                    loginController.mobile = val;
                  }),
              TextButton(
                onPressed: () async {
                  //Navigator.push(context,
                  //  MaterialPageRoute(builder: (context) => OtpScreen()));
                  if (loginController.phoneController.text.length != 10) {
                    Get.snackbar('error', 'please enter 10 digit mobile no.');
                  }

                  if (loginController.phoneController.text.length == 10) {
                    await otpController.auth.verifyPhoneNumber(
                      phoneNumber: '+91' + loginController.phoneController.text,
                      verificationCompleted: (phoneAuthCredential) async {},
                      verificationFailed: (verificationFailed) async {
                        Get.snackbar('error', verificationFailed.message);
                      },
                      codeSent: (verificationId, resendingToken) async {
                        await SmsAutoFill().listenForCode;

                        Get.to(()=>OtpScreen());
                        
                        loginController.verificationId = verificationId;
                      },
                      codeAutoRetrievalTimeout: (verificationId) async {},
                    );
                  }
                },
                child: Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  height: 40.h,
                  color: Colors.green[300],
                  child: Text(
                    "Continue",
                    style: TextStyle(
                        fontSize: 20.sp,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
              ),
              SizedBox(
                height: 5.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "By Signing up you agree to ",
                    style: TextStyle(fontSize: 12.sp),
                  ),
                  Text(
                    "Terms of use",
                    style: TextStyle(color: Colors.green, fontSize: 12.sp),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
