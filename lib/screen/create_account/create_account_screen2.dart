import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hanchens/constants.dart';
import 'package:hanchens/controllers/camera_controller.dart';
import 'package:hanchens/controllers/create_account_controller.dart';
import 'package:hanchens/screen/camera/back_image.dart';
import 'package:hanchens/screen/camera/front_image.dart';
import 'package:hanchens/screen/create_account/components/create_account_button.dart';
import 'package:hanchens/screen/create_account/components/documents.dart';
import 'package:hanchens/screen/create_account/create_account_screen3.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'components/driver_details.dart';

class CreateAccountPage2 extends StatelessWidget {
  CreateAccountPage2({Key key}) : super(key: key);

  // final CreateAccountController createAccountController =
  //   Get.find<CreateAccountController>();
  final CreateAccountController createAccountController =
      Get.put(CreateAccountController());
  final CameraController cameraController = Get.put(CameraController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(),
        centerTitle: true,
        title: const Text(
          'Create account',
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Divider(
                color: Colors.grey[200],
              ),
              SizedBox(
                height: 10.h,
              ),
              Text(
                createAccountController.vehiclesDetails,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10.h,
              ),
              const StepProgressIndicator(
                padding: 0,
                totalSteps: 4,
                currentStep: 2,
                selectedColor: Colors.green,
                unselectedColor: Colors.grey,
              ),
              SizedBox(
                height: 30.h,
              ),
              Text(
                'Vehicle details',
                style: TextStyle(fontSize: 18.sp, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20.h,
              ),
              DriverDetails(
                controller: createAccountController.vehicleNumberController,
                heading: createAccountController.vehicleNumber,
              ),
              SizedBox(
                height: 10.h,
              ),
              Row(
                children: [
                  ImageWidget(),
                  SizedBox(
                    width: 10.w,
                  ),
                  ImageWidget2(),
                ],
              ),
              SizedBox(
                height: 10.h,
              ),
              DriverDetails(
                controller: createAccountController.rcNumberController,
                heading: createAccountController.rcNumber,
              ),
              Documents(
                docType: 'rc number',
                onPressed: () {},
              ),
              SizedBox(
                height: 20.h,
              ),
              Text(
                createAccountController.insuraceExpiryDate,
                style: textHeadingStyle,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Obx(
                    () => Text(
                      createAccountController.formattedInsuranceExpiryDate.value
                          .toString(),
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      createAccountController.selectDate(context, 'insurance');
                    },
                    icon: const Icon(Icons.calendar_today_rounded),
                  ),
                ],
              ),
              Documents(
                docType: 'Insurance',
                onPressed: () {},
              ),
              SizedBox(
                height: 20.h,
              ),
              Text(
                createAccountController.dlExpiryDate,
                style: textHeadingStyle,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Obx(
                    () => Text(
                      createAccountController.formattedDlExpiryDate.value
                          .toString(),
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      createAccountController.selectDate(context, 'DL');
                    },
                    icon: const Icon(Icons.calendar_today_rounded),
                  ),
                ],
              ),
              Documents(
                docType: 'Insurance',
                onPressed: () {},
              ),
              DriverDetails(
                  controller: createAccountController.vehicleTypeController,
                  heading: createAccountController.vehicleType),
              CreateAccountButton(
                onPressed: () {
                  Get.to(() => CreateAccountPage3());
                },
                text: 'Next',
              ),
              SizedBox(
                height: 20.h,
              )
            ],
          ),
        ),
      ),
    );
  }
}
