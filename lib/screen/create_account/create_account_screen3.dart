import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hanchens/controllers/create_account_controller.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import 'components/driver_details.dart';
import 'components/radio_button.dart';

class CreateAccountPage3 extends StatelessWidget {
  CreateAccountPage3({Key key}) : super(key: key);
  final CreateAccountController createAccountController =
      Get.put(CreateAccountController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(),
        centerTitle: true,
        title: const Text(
          'Create account',
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 10.w,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Divider(
                color: Colors.grey[200],
              ),
              SizedBox(
                height: 10.h,
              ),
              Text(
                createAccountController.workDetails,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10.h,
              ),
              const StepProgressIndicator(
                padding: 0,
                totalSteps: 4,
                currentStep: 3,
                selectedColor: Colors.green,
                unselectedColor: Colors.grey,
              ),
              SizedBox(
                height: 30.h,
              ),
              Text(
                'Work details',
                style: TextStyle(fontSize: 18.sp, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20.h,
              ),
              RadioButton('Worked in past ( if any )'),
              SizedBox(
                height: 10.h,
              ),
              DriverDetails(
                controller: createAccountController.companyNameController,
                heading: createAccountController.companyName,
              ),
              SizedBox(
                height: 10.h,
              ),
              DriverDetails(
                controller: createAccountController.experienceYearsController,
                heading: createAccountController.experienceYears,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
