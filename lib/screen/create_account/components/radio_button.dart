import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hanchens/controllers/radio_button_controller.dart';

// ignore: must_be_immutable
class RadioButton extends StatelessWidget {
  RadioButton(this.question, {Key key}) : super(key: key);
  RadioButtonController radioButtonController =
      Get.put(RadioButtonController());
  final String question;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          question,
          style: const TextStyle(
              fontSize: 18, fontWeight: FontWeight.w500, color: Colors.black87),
        ),
        SizedBox(
          height: 20.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                radioButtonController.pressButton2();
              },
              child: Row(
                children: [
                  Obx(
                    () => CircleAvatar(
                      radius: 10,
                      backgroundColor: radioButtonController.button1.value == 0
                          ? Colors.green
                          : Colors.black,
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 8,
                        child: CircleAvatar(
                          backgroundColor:
                              radioButtonController.button1.value == 0
                                  ? Colors.green
                                  : Colors.white,
                          radius: 5,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  const Text(
                    'Yes',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            SizedBox(
              width: 50.w,
            ),
            InkWell(
              onTap: () {
                radioButtonController.pressButton1();
              },
              child: Row(
                children: [
                  Obx(
                    () => CircleAvatar(
                      radius: 10,
                      backgroundColor: radioButtonController.button2.value == 0
                          ? Colors.green
                          : Colors.black,
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 8,
                        child: CircleAvatar(
                          backgroundColor:
                              radioButtonController.button2.value == 0
                                  ? Colors.green
                                  : Colors.white,
                          radius: 5,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  const Text(
                    'no',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            )
          ],
        )
      ],
    );
  }
}
