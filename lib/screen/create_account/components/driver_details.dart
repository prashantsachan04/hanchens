import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../constants.dart';

class DriverDetails extends StatelessWidget {
  const DriverDetails(
      {Key key, @required this.controller, @required this.heading})
      : super(key: key);

  final TextEditingController controller;
  final String heading;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(heading, style: textHeadingStyle),
          TextField(
            decoration: InputDecoration(
              prefixText: heading == 'MOBILE NUMBER' ? '+91 ' : '',
              prefixStyle: const TextStyle(color: Colors.black),
              isDense: true,
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.green[300]),
              ),
            ),
            controller: controller,
          )
        ],
      ),
    );
  }
}
