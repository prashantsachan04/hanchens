import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Documents extends StatelessWidget {
  const Documents({
    Key key,
    this.docType,
    this.onPressed,
  }) : super(key: key);
  final String docType;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[100],
      width: double.infinity,
      height: 100.h,
      child: Row(
        children: [
          Container(color: Colors.blueGrey, width: 160.w),
          SizedBox(
            width: 160.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  docType,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                ElevatedButton(
                  onPressed: onPressed,
                  child: const Text('Upload New'),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
