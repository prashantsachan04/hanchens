import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hanchens/screen/create_account/components/radio_button.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:hanchens/constants.dart';
import 'package:hanchens/controllers/create_account_controller.dart';
import 'package:hanchens/screen/create_account/components/driver_details.dart';
import 'package:hanchens/screen/create_account/create_account_screen2.dart';
import 'components/create_account_button.dart';
import 'components/documents.dart';

class CreateAccountPage1 extends StatelessWidget {
  final CreateAccountController createAccountController =
      Get.put(CreateAccountController());
  CreateAccountPage1({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(),
        centerTitle: true,
        title: const Text(
          'Create account',
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Divider(
                color: Colors.grey[200],
              ),
              SizedBox(
                height: 10.h,
              ),
              Text(
                createAccountController.personalDetails,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10.h,
              ),
              const StepProgressIndicator(
                padding: 0,
                totalSteps: 4,
                currentStep: 1,
                selectedColor: Colors.green,
                unselectedColor: Colors.grey,
              ),
              SizedBox(
                height: 20.h,
              ),
              Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.w),
                    child: CircleAvatar(
                      backgroundColor: Colors.blue[300],
                      radius: 60,
                      child: const Icon(
                        Icons.account_box_rounded,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    // color: Colors.white,
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(1000))),
                    padding: EdgeInsets.all(3.w),
                    child: CircleAvatar(
                      backgroundColor: Colors.green[300],
                      child: IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.camera_alt,
                            color: Colors.white,
                          )),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 20.h,
              ),
              Text(
                'Personal details',
                style: TextStyle(fontSize: 18.sp, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20.h,
              ),
              DriverDetails(
                controller: createAccountController.nameController,
                heading: createAccountController.name,
              ),
              DriverDetails(
                controller: createAccountController.emailController,
                heading: createAccountController.email,
              ),
              DriverDetails(
                controller: createAccountController.mobileController,
                heading: createAccountController.mobile,
              ),
              DriverDetails(
                  controller:
                      createAccountController.permanentAddressController,
                  heading: createAccountController.permanentAddress),
              DriverDetails(
                controller: createAccountController.currentAddressController,
                heading: createAccountController.currentAddress,
              ),
              DriverDetails(
                controller: createAccountController.aadhaarController,
                heading: createAccountController.aadhaar,
              ),
              Documents(
                docType: 'Aadhaar card',
                onPressed: () {},
              ),
              DriverDetails(
                controller: createAccountController.panController,
                heading: createAccountController.pan,
              ),
              Documents(
                docType: 'Pan card',
                onPressed: () {},
              ),
              SizedBox(
                height: 10.h,
              ),
              Text(
                createAccountController.policVerification,
                style: textHeadingStyle,
              ),
              SizedBox(
                height: 10.h,
              ),
              RadioButton('Local Address mathces KYC document'),
              SizedBox(
                height: 10.h,
              ),
              Obx(
                () => DropdownButton<String>(
                  autofocus: true,
                  isExpanded: true,
                  value: createAccountController.verificationDoc.value,
                  //elevation: 5,
                  style: const TextStyle(color: Colors.black),

                  items: createAccountController.list.toList(),

                  onChanged: (String value) {
                    // print(value + 'nigaaaa');

                    createAccountController.verificationDoc.value = value;
                  },
                ),
              ),
              SizedBox(
                height: 10.h,
              ),
              Documents(
                docType: 'Police Verification',
                onPressed: () {},
              ),
              SizedBox(
                height: 10.h,
              ),
              CreateAccountButton(
                onPressed: () {
                  Get.to(() => CreateAccountPage2());
                },
                text: 'Next',
              ),
              SizedBox(
                height: 10.h,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
