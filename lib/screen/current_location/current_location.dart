// ignore_for_file: prefer_const_constructors, unused_field

import 'dart:async';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hanchens/Assitants/assistant_method.dart';
import 'package:hanchens/screen/current_location/drawer.dart';
import 'package:hanchens/screen/login_screen/login_screen.dart';
import 'package:image_picker/image_picker.dart';

class CurrentLocation extends StatefulWidget {
  static String routeName = "/Current_location";

  const CurrentLocation({Key key}) : super(key: key);

  @override
  State<CurrentLocation> createState() => _CurrentLocationState();
}

class _CurrentLocationState extends State<CurrentLocation> {
  String titleData = 'Location';
  String completeData = '';
  final Completer<GoogleMapController> _controllerGoogleMap = Completer();
  GoogleMapController newGoogleMapController;
  Position currentPosition;
  Geolocator geolocator = Geolocator();
  File _image;
  final _auth = FirebaseAuth.instance;

  Future scanImage() async {
    final image = await ImagePicker().pickImage(
      source: ImageSource.camera,
    );

    setState(() {
      _image = image as File;
    });
  }

  void locatePosition() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    LatLng latLngPosition = LatLng(position.latitude, position.longitude);

    CameraPosition cameraPosition =
        CameraPosition(target: latLngPosition, zoom: 14);
    List<String> address =
        await AssitantMethod().searchCoordinateAddress(position);
    [];
    titleData = address[0];
    completeData = address[1];

    newGoogleMapController
        .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    setState(() {
      markers.add(Marker(
          markerId: MarkerId('id-1'),
          position: latLngPosition,
          infoWindow:
              InfoWindow(title: 'Data from api', snippet: 'Data from api')));
    });
  }

  static final CameraPosition _kGooglePlex =
      CameraPosition(target: LatLng(37.4279879798, -122.085797340), zoom: 14);

  Set<Marker> markers = {};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[300],
        title: TextButton(
            onPressed: () {
              locatePosition();
              //print(title);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [Text(titleData), Icon(Icons.location_on)],
            )),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              scanImage();
            },
            icon: Icon(
              Icons.qr_code_scanner,
              size: 21,
            ),
          ),
          Padding(padding: EdgeInsets.all(10)),
          Icon(
            Icons.notification_important_rounded,
            size: 21,
          ),
          Padding(padding: EdgeInsets.all(10)),
          InkWell(
            onTap: () async {
              await _auth.signOut();
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => LoginScreenGetX()));
            },
            child: Icon(
              Icons.logout_rounded,
              size: 21,
            ),
          ),
          Padding(padding: EdgeInsets.all(10)),
        ],
      ),
      drawer: DrawerPage(),
      body: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.92,
            child: GoogleMap(
              markers: markers,
              initialCameraPosition: _kGooglePlex,
              myLocationButtonEnabled: true,
              onMapCreated: (GoogleMapController controller) {
                _controllerGoogleMap.complete(controller);
                newGoogleMapController = controller;

                //locatePosition();
              },
            ),
          ),
          /*Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              // const Icon(Icons.check_circle_outline),
              Flexible(
                child: Text(
                  completeData,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 14.sp),
                ),
              ),

              TextButton(
                onPressed: () {},
                child: Text(
                  'Change',
                  style: TextStyle(
                      color: Colors.red[400],
                      fontWeight: FontWeight.bold,
                      fontSize: 14.sp),
                ),
              )
            ],
          ),*/
        ],
      ),
    );
  }
}
