// ignore_for_file: prefer_const_constructors, unused_field

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:hanchens/screen/about_us/about_us.dart';
import 'package:hanchens/screen/login_screen/login_screen.dart';

class DrawerPage extends StatelessWidget {
  const DrawerPage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 10.h),
            userPhoneAndName(),
            SizedBox(height: 20.h),
            creditDetails(),
            SizedBox(height: 10.h),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 10.w),
              child: drawerOptions(
                  Icon(Icons.notifications_active), 'Notification'),
            ),
            Theme(
              data: ThemeData().copyWith(dividerColor: Colors.transparent),
              child: ExpansionTile(
                childrenPadding:
                    EdgeInsets.symmetric(vertical: 10.w, horizontal: 10.w),
                initiallyExpanded: true,
                title: Text(
                  'My Account',
                  textAlign: TextAlign.start,
                ),
                children: [
                  drawerOptions(Icon(Icons.settings), 'Settings'),
                  drawerOptions(Icon(Icons.bookmark), 'My Favourites'),
                  drawerOptions(Icon(Icons.food_bank), 'My Orders'),
                  drawerOptions(
                      Icon(Icons.location_on_rounded), 'Saved Addresses'),
                ],
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
            Theme(
              data: ThemeData().copyWith(dividerColor: Colors.transparent),
              child: ExpansionTile(
                childrenPadding:
                    EdgeInsets.symmetric(vertical: 10.w, horizontal: 10.w),
                initiallyExpanded: true,
                title: Text('My Hanchens'),
                children: [
                  InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, AboutUs.routeName);
                      },
                      child: drawerOptions(
                          Icon(Icons.info_outline_rounded), 'About us')),
                  drawerOptions(
                      Icon(Icons.file_present_sharp), 'Terms & Conditions'),
                  drawerOptions(Icon(Icons.privacy_tip), 'Privacy Policy'),
                  drawerOptions(Icon(Icons.help), 'Help'),
                  drawerOptions(Icon(Icons.query_builder), "FAQ's"),
                ],
              ),
            ),
            referAndEarn(),
            Divider(
              color: Colors.black,
            ),
            InkWell(
              onTap: () {
                Get.to(LoginScreenGetX());
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.logout_rounded,
                  ),
                  Text('Logout')
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Padding referAndEarn() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      child: Container(
        height: 30.h,
        width: double.infinity.w,
        color: Colors.green[300],
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Refer and earn'),
            SizedBox(
              width: 60.w,
            ),
            Icon(Icons.money_rounded)
          ],
        ),
      ),
    );
  }

  Row drawerOptions(Icon icon, String title) {
    return Row(
      children: [
        icon,
        SizedBox(
          width: 60.w,
        ),
        Text(title),
      ],
    );
  }

  Container creditDetails() {
    return Container(
      height: 60.h,
      decoration: BoxDecoration(border: Border.all(color: Colors.black)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Rs 500',
                style: TextStyle(
                    color: Colors.green,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.sp),
              ),
              SizedBox(height: 10.h),
              Text(
                'Credit balance',
                style: TextStyle(fontSize: 13.sp),
              )
            ],
          ),
          Icon(
            Icons.wallet_membership,
            size: 30.sp,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Rs 1000',
                style: TextStyle(
                    color: Colors.green,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.sp),
              ),
              SizedBox(height: 10.h),
              Text(
                'Credit consumed',
                style: TextStyle(fontSize: 13.sp),
              )
            ],
          )
        ],
      ),
    );
  }

  Column userPhoneAndName() {
    return Column(
      children: const [
        Icon(
          Icons.account_circle_outlined,
          size: 60,
        ),
        Text(
          "Hi! User Name",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}
